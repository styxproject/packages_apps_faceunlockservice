package com.motorola.faceunlock.camera.listeners;

public interface CallableEventListener {
    void onEventCallback(int i, Object value);
}
